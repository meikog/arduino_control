void bremspedal() {
  multi_brake();
  //Steuerung der Bremse
  if (bremse_debugger_an) {                                                         //Wenn Debugger für Bremse an ist steuert nur dieser!
    bremsemotor.write(bremse_target_debugger);
  }else {
      if (flag2 && wert_adc1 <= 251  && wert_adc2 <= 126 && (bremse_raw > 0 || brems_stellung_smooth > 10) ) { //Wenn Tempomat an ist und Gaspedal nicht benutzt wird und Bremse betätigt werden muss
        
        bremsemotor.write(map(constrain(brems_stellung_smooth* last_brakemulti,0,100), 0, 100, EEPROM[storage_brake_min_pos], bremse_max));
      }else{                                                                        //Wenn Tempomat aus ist, wird Die Bremse auf minimum gestellt.
        bremsemotor.write(EEPROM[storage_brake_idle_pos]);
        //bremsemotor.detach();
      }
  }
}

void multi_brake(){
//# Multiplikation für Bremse#

if(bremse > 1 && bremse <= 200){
last_brakemulti  = (float)EEPROM[storage_brakem200] / 100.0;
}

if(bremse > 200 && bremse <= 400){
last_brakemulti = (float)EEPROM[storage_brakem400] / 100.0;
}

if(bremse > 400 && bremse <= 600){
last_brakemulti = (float)EEPROM[storage_brakem600] / 100.0;
}

if(bremse > 600 && bremse <= 800){
last_brakemulti = (float)EEPROM[storage_brakem800] / 100.0;
}

if(bremse > 800 && bremse <= 1000){
last_brakemulti = (float)EEPROM[storage_brakem1000] / 100.0;
}

if(bremse > 1000 && bremse <= 1200){
last_brakemulti = (float)EEPROM[storage_brakem1200] / 100.0;
}

if(bremse > 1200 && bremse <= 1400){
last_brakemulti = (float)EEPROM[storage_brakem1400] / 100.0;
}

if(bremse > 1400 && bremse <= 1600){
last_brakemulti = (float)EEPROM[storage_brakem1600] / 100.0;
}

if(bremse > 1600 && bremse <= 1800){
last_brakemulti = (float)EEPROM[storage_brakem1800] / 100.0;
}

if(bremse > 1800){
last_brakemulti = (float)EEPROM[storage_brakem2000] / 100.0;
}
  
}
