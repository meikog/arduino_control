void debugger_page_name(){
  
  nextion_send(   F("db1.txt=\""),        F("LOOP MAIN / OP INTER")     ,   F("\""));
  nextion_send(   F("db2.txt=\""),        F("ADC1 / DAC1")              ,   F("\""));
  nextion_send(   F("db3.txt=\""),        F("GAS RAW / CA AA")          ,   F("\""));
  nextion_send(   F("db4.txt=\""),        F("BREMSE RAW / Lookup")      ,   F("\""));
  nextion_send(   F("db5.txt=\""),        F("GAS % R/ S")               ,   F("\""));
  nextion_send(   F("db6.txt=\""),        F("BREMSE % R/S")             ,   F("\""));
  nextion_send(   F("db7.txt=\""),        F("LEAD_LONG_DIST")           ,   F("\""));
  nextion_send(   F("db8.txt=\""),        F("LEAD_REL_SPEED")           ,   F("\""));
  nextion_send(   F("db9.txt=\""),        F("IDLE CORRECTION")          ,   F("\""));
  
  

}

void update_display(){
  last_display_update = millis();

  nextion_send(   F("eingangadc1.txt=\""),    String(wert_adc1)+" V:" + (0.00488*wert_adc1) ,   F("\""));
  nextion_send(   F("eingangadc2.txt=\""),    String(wert_adc2)+" V:" + (0.00488*wert_adc2) ,   F("\""));
  
  nextion_send(   F("gas.val="),              String(gas_stellung_smooth)                   ,   F(""));
  nextion_send(   F("bremse.val="),           String(brems_stellung_smooth)                        ,   F(""));
  
  nextion_send(   F("bremstxt.txt=\""),       String(bremse_raw)                            ,   F("\""));
  nextion_send(   F("gastxt.txt=\""),         String(gas_von_ecu_can_raw)                   ,   F("\""));

  nextion_send(   F("dbremsetxt.txt=\""),     String(bremse_target_debugger)                ,   F("\""));
    
  nextion_send(   F("dba1.txt=\""),       String(ausgabe_loop_sec)      + " / " + String(ausgabe_int_sec)                                 ,   F("\""));
  nextion_send(   F("dba2.txt=\""),       String(wert_adc1)             + " / " + String(wert_dac1)                                       ,   F("\""));
  nextion_send(   F("dba3.txt=\""),       String(gas_von_ecu_can_raw)   + " / " + String(CA)                    + " / " + tempomatadc1    ,   F("\""));
  nextion_send(   F("dba4.txt=\""),       String(bremse_raw)            + " / " + String(bremse_2d)                                       ,   F("\""));
  nextion_send(   F("dba5.txt=\""),       String(gas_stellung_raw)      + " / " + String(gas_stellung_smooth)                             ,   F("\""));
  nextion_send(   F("dba6.txt=\""),       String(brems_stellung_raw)    + " / " + String(brems_stellung_smooth)                           ,   F("\""));
  nextion_send(   F("dba7.txt=\""),       String(LEAD_LONG_DIST)                                                                          ,   F("\""));
  nextion_send(   F("dba8.txt=\""),       String(LEAD_REL_SPEED)                                                                          ,   F("\"")); 
  nextion_send(   F("dba9.txt=\""),       String(idle_correctur)                                                                          ,   F("\""));

  nextion_send(   F("brmulti.txt=\""),    String(last_brakemulti)                                                                         ,   F("\""));

  
  nextion_send(   F("xxtxt.txt=\"")     ,  String(LEAD_REL_SPEED)        ,   F("\""));
  nextion_send(   F("lead_dist.txt=\"") ,  String(LEAD_LONG_DIST)        ,   F("\""));
  
  nextion_change_speed_ui();

}

void update_brk_display(){ 
  //DEBUGGER Page!
  //BREMSENDEBUGGER
  nextion_send(   F("br200a.txt=\""),         String(EEPROM[storage_brakem200])       ,   F("\""));
  nextion_send(   F("db400a.txt=\""),         String(EEPROM[storage_brakem400])       ,   F("\""));
  nextion_send(   F("db600a.txt=\""),         String(EEPROM[storage_brakem600])       ,   F("\""));
  nextion_send(   F("db800a.txt=\""),         String(EEPROM[storage_brakem800])       ,   F("\""));
  nextion_send(   F("db1000a.txt=\""),        String(EEPROM[storage_brakem1000])      ,   F("\""));
  nextion_send(   F("db1200a.txt=\""),        String(EEPROM[storage_brakem1200])      ,   F("\""));
  nextion_send(   F("db1400a.txt=\""),        String(EEPROM[storage_brakem1400])      ,   F("\""));
  nextion_send(   F("db1600a.txt=\""),        String(EEPROM[storage_brakem1600])      ,   F("\""));
  nextion_send(   F("db1800a.txt=\""),        String(EEPROM[storage_brakem1800])      ,   F("\""));
  nextion_send(   F("db200a.txt=\""),         String(EEPROM[storage_brakem2000])      ,   F("\""));
  nextion_send(   F("dbbrkmax.txt=\""),       String((EEPROM[storage_brake_max]*10))  ,   F("\""));

  nextion_send(   F("brems_min_pos.txt=\""),  String((EEPROM[storage_brake_min_pos]))  ,   F("\""));
  nextion_send(   F("brems_idle_pos.txt=\""), String((EEPROM[storage_brake_idle_pos]))  ,   F("\""));
}

void nextion_send(String a, String b, String c) {
  Serial.write(0xff);
  Serial.write(0xff);
  Serial.write(0xff);
  Serial.print(a+b+c);
  Serial.write(0xff);
  Serial.write(0xff);
  Serial.write(0xff);
  Serial.println("");
}


void nextionspeed(String befehl) {
  nextion_send("geschwin.txt=\"", String(befehl), F("\"") );
}

void nextionbremse_debugger(byte befehl) {
  nextion_send("dbremsetxt.txt=\"", String(befehl), F("\"") );
}
//Ändern der Geschwindigkeit auf dem Display
void nextion_change_speed_ui() {
    nextionspeed(String(set_speed));
}
