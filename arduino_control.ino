//V0.4
//#####################################
//#####################################

#define storage_brakem200   513
#define storage_brakem400   514
#define storage_brakem600   515
#define storage_brakem800   516
#define storage_brakem1000  517
#define storage_brakem1200  518
#define storage_brakem1400  519
#define storage_brakem1600  520
#define storage_brakem1800  521
#define storage_brakem2000  522

#define storage_brake_max   523

#define storage_brake_min_pos     524
#define storage_brake_idle_pos    525

typedef unsigned char uint8_t;
//#include <SoftwareSerial.h>
//#include <TimerOne.h>

#include <Wire.h>
#include <avr/wdt.h>
#include <SPI.h>
#include "mcp_can.h"
#include <Servo.h>
#include <Adafruit_MCP4728.h>
#include <EEPROM.h>

MCP_CAN CAN_CAR(10);                       //CAN_CAR
MCP_CAN CAN_OP(9);                        //CAN_OP
Adafruit_MCP4728 sensordac;
Servo bremsemotor;

long         endless_loop_count    = 0;
float        loop_count            = 0;
float        hundert_hz_loop_delay      = 0;

float        last_loop_calc        = 0;
unsigned int ausgabe_loop_sec      = 0;



//-----------CAN-MESSEGAS-------------------------
//------------------------------------------------

float           interrupt_counter     = 0;
unsigned int    ausgabe_int_sec       = 0;
uint8_t         x200_cansignal[8];            //GAS
uint8_t         x343_cansignal[8];            //BREMSE
uint8_t         x2E6_cansignal[8];            //BREMSE

byte            other_cansignal_op[8];
byte            other_cansignal_car[8];
byte            x348_car_cansignal[8];        //GM SPEED

uint16_t        kmh_raw;    //16bit x 0,0311 = KMH
float           kmh_echt;   //KMH

byte            len = 0;

//-----------Variablen: SEND FAKE MSG-------------
//------------------------------------------------
//flags for cruise state
boolean         flag1 = true;
boolean         flag2             = false;
boolean         reset_tmp_op      = false; // SHORT RESET OP TO RESET PID AT FAST CLOSER LEAD!

//pedal State
const int       gas_pedal_state   = 1;
const int       brake_pedal_state = 1;

//CAN default messages
uint8_t         set_speed = 130;
byte            addr[] = {0xfd, 0xfe, 0xff};
byte            data[] = {0x0, 0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7};
const int       LKA_STATE = 5;
const int       TYPE = 0;

//-----------COMMA UPDATERATE---------------------
//------------------------------------------------
String      nextionstring = "";
const float refresh_comma_interval    = 150;  //IN MS
float       last_comma_update         = 0;

//-----------DISPLAY UPDATERATES-------------------
//------------------------------------------------
const float refresh_display_interval  = 250;  //IN MS
float       last_display_update       = 0;

//-----------Variablen: Zur Berechnung------------
//------------------------------------------------
//float     gas_von_ecu_can         = 0;          //Der Wert der vom Steuergerät ausgegeben wird auf dem die Gaspedalstellung gesetzt wird ca. 0-1116
uint16_t    gas_von_ecu_can_raw     = 0;          //Der Wert der vom Steuergerät ausgegeben wird auf dem die Gaspedalstellung gesetzt wird ca. 0-1116
float       gas_stellung_raw        = 0;          //Berechneter Wert für Gasstellung Wert von 0-100
float       gas_stellung_smooth     = 0;          //Berechneter Wert für Gasstellung Wert von 0-100
float       gas_voltage_corr        = 0;          //Da Gas nicht linear ist wird die Spannung als Kurve berechnet
float       gas_2d_multi            = 1;

uint16_t    bremse_raw              = 0;          //Der Wert der vom Steuergerät ausgegeben wird auf dem die Bremsstellung gesetzt wird ca. 0-2500
float       bremse                  = 0;          //Der Wert der vom Steuergerät ausgegeben wird auf dem die Bremsstellung gesetzt wird ca. 0-2500
int         brems_stellung_raw      = 0;          //Berechneter Wert für Bremsstellung Wert von 0-100s
int         brems_stellung_smooth   = 0;          //raw wird gesmooth
byte        bremse_2d               = 0;

float       LEAD_LONG_DIST          = 0;
float       LEAD_REL_SPEED          = 0;
int16_t     LEAD_LONG_DIST_RAW      = 0;
int16_t     LEAD_REL_SPEED_RAW      = 0;

int         average                 = 50;         //Initialgeschwindigkeit bevor Sie neu übertragen wird in km/h

unsigned long lastmillis  = 0;

//Pedalsteuerung

//Blau + Schwarz = Channel C
static const int   read_adc1    = A3;
static const int   write_dac1   = 2;
//Rot + Grün = Channel A 
static const int   read_adc2    = A2;
static const int   write_dac2   = 0;

int         wert_adc1           = 193;        //Ausgelesenen Werte vom Gaspedal1
int         wert_adc2           = 93;         //Ausgelesenen Werte vom Gaspedal2

int         wert_dac1           = 4095;       //Ausgabewert DAC
int         wert_dac2           = 2050;       //Ausgabewert DAC
String      tempomatadc1        = "off";
String      idle                = "default";

int         CA                  = 0;
int         AA                  = 0;

//MCP4278
static const float idle_voltage_pedal1     = 950;
static const float idle_voltage_pedal2     = 460;

long  idle_correction_storage = 512;      //GAS 
int   idle_correctur;                     //GAS


//Bremse
int                      bremse_target;
bool                     bremse_debugger_an      = false;
static const int         bremse_max              = 18;
//static const int         bremse_min              = 93;  //deprecated moved to variable storage room 524
//static const int         bremse_idle             = 145; //deprecated moved to variable storage room 525
int                      bremse_target_debugger  = EEPROM[storage_brake_idle_pos];

float last_brakemulti					= 100; // Wert wird durch 100 geteilt z.B. 100 = Multi 1x; 1 = Multi 0,01!

static const bool enable_master_for_develop = true;

static const bool  enable_car_can = enable_master_for_develop;
static const bool  enable_op_can  = enable_master_for_develop;
static const bool  enable_mcp4728 = enable_master_for_develop;


float table_meter_arr[]  = {0, 5, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 120, 130, 140, 150};
float table_relat_arr[]  = {-3.5 ,-3, -2.5, -2, -1.5, -1, -0.5, 0, 0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4};

//Wert gibt bremsstellung in % raus
byte table_meter_rel[16][16]  = {
//0   5   10  20  30  40  50  60  70  80  90 100 110 120 130 140  Meter / relativ  %

{100,100, 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 } , //  -3.5          256   - 271
{100,100, 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 } , //  -3            272   - 287
{100,100, 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 } , //  -2.5          288   - 303
{100,100, 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 } , //  -2            304   - 319
{100,100, 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 } , //  -1.5          320   - 335
{100,100, 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 } , //  -1            336   - 351
{100,100, 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 } , //  -0.5          352   - 367
{100,100, 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 } , //   0            368   - 383
{100,100, 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 } , //   0.5          384   - 399
{100,100, 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 } , //   1            400   - 415
{100,100, 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 } , //   1.5          416   - 431  
{100,100, 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 } , //   2            432   - 447
{100,100, 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 } , //   2.5          448   - 463
{100, 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 } , //   3            464   - 479
{100, 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 } , //   3.5          480   - 495
{100, 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 }   //   4            496   - 511

};

byte table_speed_arr[] = {0,3,5,30,40,50,60,70,80,90,100,120,130,140,150,160};
byte table_last_arr[]  = {0,1,3,10,15,20,25,30,35,40,50,60,70,80,90,100};

//Wert wird durch 100 geteilt und bildet multiplikator in gas! 100 = 1
byte table_last_speed[16][16]  = {
//0   3  20  30  40  50  60  70  80  90 100 110 120 130 140 150  KMH / LAST %

{  1,1,100,100,100,100,100,100,100,100,100,100,100,100,100,100}  , //  0    0     - 15
{  1,1,100,100,100,100,100,100,100,100,100,100,100,100,100,100} , //  1     16    - 31  
{  1,1,100,100,100,100,100,100,100,100,100,100,100,100,100,100} , //  5     32    - 47
{  1,1,100,100,100,100,100,100,100,100,100,100,100,100,100,100} , //  10    48    - 63
{  1,1,100,100,100,100,100,100,100,100,100,100,100,100,100,100} , //  15    64    - 79 
{  1,1,100,100,100,100,100,100,100,100,100,100,100,100,100,100} , //  20    80    - 95
{  1,1,100,100,100,100,100,100,100,100,100,100,100,100,100,100} , //  25    96    - 111
{  1,1,100,100,100,100,100,100,100,100,100,100,100,100,100,100} , //  30    110   - 127
{  1,1,100,100,100,100,100,100,100,100,100,100,100,100,100,100} , //  35    126   - 143
{  1,1,100,100,100,100,100,100,100,100,100,100,100,100,100,100} , //  40    142   - 159
{  1,1,100,100,100,100,100,100,100,100,100,100,100,100,100,100} , //  50    158   - 175
{  1,1,100,100,100,100,100,100,100,100,100,100,100,100,100,100} , //  60    174   - 191
{  1,1,100,100,100,100,100,100,100,100,100,100,100,100,100,100} , //  70    190   - 207
{  1,1,100,100,100,100,100,100,100,100,100,100,100,100,100,100} , //  80    206   - 223
{  1,1,100,100,100,100,100,100,100,100,100,100,100,100,100,100} , //  90    222   - 239
{  1,1,100,100,100,100,100,100,100,100,100,100,100,100,100,100}   //  100   238   - 255
};

//KONSOLE Kommunikation

boolean update_display_enable = true; // IF enter help this will be disable; if enter enable this will be true!
//###############################
//######BEGINN###################
//##############################
void setup(void) {
  
  Serial.setTimeout(10);                        //setze Timeout auf 10MS damit der Prozess schnell weiter geht
  //Serial.begin(115200);
  Serial.begin(115200);
  //wdt_enable(WDTO_2S);
  //-----------DAC 4728 AKTIVIEREN------------------
  //------------------------------------------------
  
  if(enable_mcp4728){
    while (!sensordac.begin()) {
      Serial.println(F("Failed to find MCP4728 chip"));
  }; 
    gaspedal(); 
  }
  pinMode(2, INPUT);                           //Interrupt MCP
  pinMode(read_adc1, INPUT);                   //Gaspedalsensor auslesen1 1v
  pinMode(read_adc2, INPUT);                   //Gaspedalsensor auslesen2 0.5v


  //-----------CAN-BUS SHIELD AKTIVIEREN 2x---------
  //------------------------------------------------
 

  if(enable_car_can){ 
    while (CAN_OK != CAN_CAR.begin(CAN_500KBPS)) {
      Serial.println(F("Starting CAN_CAR failed!"));
    }
    CAN_CAR.init_Mask(0, 0, 0x7FF);                         // there are 2 mask in mcp2515, you need to set both of them
    CAN_CAR.init_Mask(1, 0, 0x7FF);
  
    CAN_CAR.init_Filt(0, 0, 0x348);           //Fahrzeug Geschwindigkeit
    //CAN_CAR.init_Filt(1, 0, 0x348);           //Fahrzeug Geschwindigkeit
  }

  if(enable_op_can){ 
    while (CAN_OK != CAN_OP.begin(CAN_500KBPS)) {
      Serial.println(F("Starting CAN_OP failed!"));
    }
    CAN_OP.init_Mask(0, 0, 0x7FF);                         // there are 2 mask in mcp2515, you need to set both of them
    CAN_OP.init_Mask(1, 0, 0x7FF);

    CAN_OP.init_Filt(0, 0, 0x343);           //Bremse
    CAN_OP.init_Filt(1, 0, 0x200);           //Gas
    CAN_OP.init_Filt(2, 0, 0x2E6);           //LEAD CAR
  }
  attachInterrupt(digitalPinToInterrupt(2), MCP2515_ISR, FALLING); // start interrupt

  //Correction IDLE
  if (EEPROM[idle_correction_storage] == 0 || EEPROM[idle_correction_storage] == 255){
    EEPROM[idle_correction_storage] = 127;
  }
  idle_correctur = EEPROM[idle_correction_storage] - 127;  
  
  //Brake_MAX_LIVE_TUNING
  if (EEPROM[storage_brake_max] == 0 || EEPROM[storage_brake_max] == 255){
    EEPROM[storage_brake_max] = 200; // MULTIPLIKATION BY 10
  }
  
  bremsemotor.attach(3);
}

void MCP2515_ISR() {

    //auslesen_can_bus_OP();
    //auslesen_can_bus_CAR();

}

void loop() {
  
  wdt_reset();                                //RESET des Watchdog, ansonsten neustart
  auslesen_can_bus_OP();                      //lesen von Daten auf dem Can-Bus von OP
  auslesen_can_bus_CAR();                     //lesen von Daten auf dem Can-Bus von CAR
  serialrw();                                 //NEXTION Display Kommunikation Auslesen von Buttons und Schaltern
  adc_read();                                 //Liest die ADC Werte vom Gaspedalsensor aus
  auslesen_can_bus_OP();                      //lesen von Daten auf dem Can-Bus von OP
  auslesen_can_bus_CAR();                     //lesen von Daten auf dem Can-Bus von CAR
  gaspedal();                                 //Gaspedal setzen, Beschleunigung oder idle
  bremspedal();                               //Bremspedal setzen, fahren oder bremsens
  table_speed_last(kmh_echt, gas_stellung_smooth);   //2D Tabelle für Gaspedal zum Tunen

  if((last_comma_update+refresh_comma_interval)< millis()){
    sendfakemsg_comma();                  //Sendet nachrichten an Comma-Dongle, das meiste ist Fake!  
    last_comma_update = millis();
  }
  
  if((last_display_update+refresh_display_interval)< millis()){
    if(update_display_enable){            //Nextion Display Update
      update_display(); 
    }
    
    //Serial.println("###DISPLAYUPDATE###");
  }
  auslesen_can_bus_OP();                      //lesen von Daten auf dem Can-Bus von OP
  auslesen_can_bus_CAR();                     //lesen von Daten auf dem Can-Bus von CAR

//##########    Workaround to prevent Brake to late      ################

//IF LEAD COME FAST CLOSE RESTART ACC TO RESET PID!

if((LEAD_REL_SPEED > -1 && reset_tmp_op == true) || flag2 == 0 ){
 reset_tmp_op = false;
}

if(flag2 == 1 && LEAD_REL_SPEED < -8 && reset_tmp_op==false ){
reset_tmp_op = true;

  //________________0x1d2 msg PCM_CRUISE
  uint8_t fmsg_pcm[8];
  fmsg_pcm[0] = (0 << 5) & 0x20 | (!gas_pedal_state << 4) & 0x10;
  fmsg_pcm[1] = 0x0;
  fmsg_pcm[2] = 0x0;
  fmsg_pcm[3] = 0x0;
  fmsg_pcm[4] = 0x0;
  fmsg_pcm[5] = 0x0;
  fmsg_pcm[6] = (0 << 7) & 0x80;
  fmsg_pcm[7] = can_cksum(fmsg_pcm, 7, 0x1d2);
  CAN_OP.sendMsgBuf(0x1d2,0,8, fmsg_pcm);
  delay(500);
}

//##########    Bremse      ################
bremse_raw  = ((x343_cansignal[0] << 8 | x343_cansignal[1] << 0) * -1); 

//SELF BRAKE at near distance; SOMETIMES OP WILL NOT BRAKE AT CLOSE TO LEAD CAR
if( LEAD_LONG_DIST > 0.2 && LEAD_LONG_DIST < 10 ){
  if (map_f(LEAD_LONG_DIST, 2, 10, 1199, 0) > bremse_raw){
    bremse_raw = map_f(LEAD_LONG_DIST, 2, 10, 1199, 0); 
  }
}


// LIMIT Brake at full STOP
if (bremse_raw > 1200 && kmh_echt < 10){
  bremse_raw = 1200;
}

if(bremse_raw > 3000 || bremse_raw < 20){              
  bremse_raw                = 0;
  //brems_stellung_smooth   = 0;
  brems_stellung_raw        = 0;
}else{
  bremse                = constrain(bremse_raw, 0, (EEPROM[storage_brake_max]*10)); //Limit Brake raw  
  brems_stellung_raw    = (bremse*100)/(EEPROM[storage_brake_max]*10);              // Zielwert
}


// ROUND TIME ~ 100 per Second
//BRAKE IDLE =145 START 80 TO 18 0-2200

//BRAKE //FROM 0 TO 2200
if (true){//endless_loop_count % 3 == 0
    if ( brems_stellung_smooth < brems_stellung_raw){
      brems_stellung_smooth = brems_stellung_smooth + 1;
    }
    if ( brems_stellung_smooth > brems_stellung_raw){
      brems_stellung_smooth = brems_stellung_smooth - 1;
    }
}

//##########      Gas      ################

//GAS FROM 0 TO 1400
if (gas_von_ecu_can_raw <= 600){ 
  gas_stellung_smooth = gas_von_ecu_can_raw;
}
if (gas_von_ecu_can_raw > 600 && gas_stellung_smooth < 600 ){
  gas_stellung_smooth = 600;
}
if ( gas_von_ecu_can_raw <=1100 && gas_stellung_smooth <= 1100 ){ // endless_loop_count % 2
    if ( gas_stellung_smooth < gas_von_ecu_can_raw){
      gas_stellung_smooth += 10;
    }
    if ( gas_stellung_smooth > gas_von_ecu_can_raw){
      gas_stellung_smooth = gas_von_ecu_can_raw;
    }
}
else
{
  if (endless_loop_count % 4 == 0){
      if ( gas_stellung_smooth > gas_von_ecu_can_raw){
          gas_stellung_smooth = gas_von_ecu_can_raw;
        }
        if ( gas_stellung_smooth < gas_von_ecu_can_raw){
          gas_stellung_smooth += 1;
      }
  }
}

gas_von_ecu_can_raw     = (x200_cansignal[0] << 8 | x200_cansignal[1]);  
gas_von_ecu_can_raw     = constrain(gas_von_ecu_can_raw, 0, 1516);        //Filtert die Gasstellung die OP liefert zwischen 0-1516 aus!        

//Wenn Bremse betätigt ist, darf kein gas gegeben werden oder selber gas gegeben wird, wird der smoother zurück gesetzt
if(brems_stellung_smooth > 0 || (wert_adc1 >= 251  && wert_adc2 >= 126) ){
  gas_stellung_smooth = 0;
}

if (gas_stellung_smooth < 544){      // Alles Unter 544 ist kein gas; 
  gas_stellung_raw         = 0;
}else{
  gas_stellung_raw        = constrain(((gas_stellung_smooth-500)*100/1516), 0, 100);  
  }

//###########  Geschwindigkeit   ##############
kmh_raw                 = (x348_car_cansignal[0] << 8 | x348_car_cansignal[1]);
kmh_echt                = kmh_raw * 0.0311;

//###########  LEAD CAR   ##################

LEAD_LONG_DIST_RAW = (x2E6_cansignal[0] << 8 | x2E6_cansignal[1] ); 
LEAD_REL_SPEED_RAW = (x2E6_cansignal[2] << 8 | x2E6_cansignal[3] );
//______________CONVERTING INTO RIGHT VALUE USING DBC SCALE
LEAD_LONG_DIST = LEAD_LONG_DIST_RAW  * 0.00625;      // Faktor 0,05   / 8 weil nur 13bit lang = Entfernung in Meter
LEAD_REL_SPEED = LEAD_REL_SPEED_RAW  * 0.005625;    // Faktor 0,025 / 16 weil nur 12bit lang * 3,6 m/s auf kmh  = KMH Lead car


//###########  CALC LOOP TIME!   ##################
if((last_loop_calc +5000) < millis()){
  last_loop_calc  = millis() - last_loop_calc;
  
  ausgabe_loop_sec  = (loop_count        *1000)   / last_loop_calc;
  ausgabe_int_sec   = (interrupt_counter *1000)   / last_loop_calc;

  loop_count        = 0;
  interrupt_counter = 0;
  last_loop_calc    = millis(); 
}

if(false)     {
  //LIMIT TO 100HZ
  //100hz_loop_delay
  if(millis() <   hundert_hz_loop_delay + 10 ){
    delay((       hundert_hz_loop_delay + 10) - millis());
  }
  hundert_hz_loop_delay = millis();
}

loop_count++;
endless_loop_count++;

}

float map_f(float x, float in_min, float in_max, float out_min, float out_max) {
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}
