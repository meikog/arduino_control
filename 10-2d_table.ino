void table_speed_last(float Geschwindigkeit, float Last){
  
if( Geschwindigkeit <0 || Geschwindigkeit > 200){
  Geschwindigkeit = 0;
}

if( Last < 0 || Last > 100){
  Last    = 0;
}

//Serial.println("2");
byte speed_1             = 0;
byte speed_2             = 0;
float verhaeltnis_1;
float verhaeltnis_2;

float verhaeltnis;

byte last_1;
float ausgabe;


//Interpoliert nur nach rechts
//last wird einfach die letzte passende reihe genommen

// finde geschwindigkeit

for (byte i=0; i<16; i = i + 1 ){

 if( Geschwindigkeit >= table_speed_arr[i]) {
  speed_1 = i ;
 }
 if( Geschwindigkeit <= table_speed_arr[i]) {
  speed_2 = i ;
  break;
 }
}

//Serial.println("speed_1");
//Serial.println(speed_1);

//Serial.println("speed_2");
//Serial.println(speed_2);


for (byte i=0; i<16; i = i + 1 ){

 if( Last >= table_last_arr[i]) {
  last_1 = i ;
 }
 if( Last <= table_last_arr[i]) {
  break;
 }
}

//Serial.println("last_1");
//Serial.println(last_1);
  

// verhältnis berechen
verhaeltnis = table_speed_arr[speed_1] / table_speed_arr[speed_1];

verhaeltnis_1 = speed_1-Geschwindigkeit ;
verhaeltnis_2 = speed_2-Geschwindigkeit ;

if(speed_1 == speed_2 || speed_2 == 0){ // Anfang einer Tabelle oder das Ende!
  gas_2d_multi = table_last_speed[last_1][speed_1];  
  
}
else{
  //gas_2d_multi = ((table_last_speed[last_1][speed_1] * verhaeltnis_2) + (table_last_speed[last_1][speed_2] * verhaeltnis_1)) /( verhaeltnis_1 + verhaeltnis_2);
  gas_2d_multi = map_f(Geschwindigkeit, speed_1, speed_2, table_last_speed[last_1][speed_1], table_last_speed[last_1][speed_2]  );
}
gas_2d_multi /= 100;
//Serial.println(F("Ergebnis"));
//Serial.println(gas_2d_multi);
  
}



void table_meter_relat(){
  


//Serial.println("2");
byte speed_1             = 0;
byte speed_2             = 0;
float verhaeltnis_1;
float verhaeltnis_2;

float verhaeltnis;

byte last_1;
float ausgabe;


//Interpoliert nur nach rechts
//last wird einfach die letzte passende reihe genommen

// finde geschwindigkeit

for (byte i=0; i<16; i = i + 1 ){

 if( kmh_echt >= table_meter_arr[i]) {
  speed_1 = i ;
 }
 if( kmh_echt <= table_meter_arr[i]) {
  speed_2 = i ;
  break;
 }
}

for (byte i=0; i<16; i = i + 1 ){

 if( LEAD_REL_SPEED  >= table_relat_arr[i]) {
  last_1 = i ;
 }
 if( LEAD_REL_SPEED  <= table_relat_arr[i]) {
  break;
 }
}

// verhältnis berechen
//verhaeltnis = table_meter_arr[speed_1] / table_meter_arr[speed_1];

verhaeltnis_1 = speed_1-kmh_echt ;
verhaeltnis_2 = speed_2-kmh_echt ;

if(speed_1 == speed_2 || speed_2 == 0){ // Anfang einer Tabelle oder das Ende!
  bremse_2d = table_meter_rel[last_1][speed_1];  
  
}
else{
  bremse_2d = ((table_meter_rel[last_1][speed_1] * verhaeltnis_2) + (table_meter_rel[last_1][speed_2] * verhaeltnis_1)) /
                ( verhaeltnis_1 + verhaeltnis_2); 
}
//Serial.println(F("Ergebnis"));
//Serial.println(gas_2d_multi);
  
}
