void sendfakemsg_comma() {
if(enable_op_can){
  //________________send Fingerprint msgs
  for (byte ii = 0; ii < 3; ii++) {
    
    //for (byte i = 0; i < 8; i++) {
      CAN_OP.sendMsgBuf(addr[ii],0,8,data);//[i]
    //}
  }

  //________________0x1d2 msg PCM_CRUISE
  uint8_t fmsg_pcm[8];
  fmsg_pcm[0] = (flag2 << 5) & 0x20 | (!gas_pedal_state << 4) & 0x10;
  fmsg_pcm[1] = 0x0;
  fmsg_pcm[2] = 0x0;
  fmsg_pcm[3] = 0x0;
  fmsg_pcm[4] = 0x0;
  fmsg_pcm[5] = 0x0;
  fmsg_pcm[6] = (flag2 << 7) & 0x80;
  fmsg_pcm[7] = can_cksum(fmsg_pcm, 7, 0x1d2);
  CAN_OP.sendMsgBuf(0x1d2,0,8, fmsg_pcm);

  //________________0x1d3 msg PCM_CRUISE_2
  uint8_t dat2[8];
  dat2[0] = 0x0;
  dat2[1] = (flag1 << 7) & 0x80 | 0x28;
  dat2[2] = set_speed;
  dat2[3] = 0x0;
  dat2[4] = 0x0;
  dat2[5] = 0x0;
  dat2[6] = 0x0;
  dat2[7] = can_cksum(dat2, 7, 0x1d3);
  CAN_OP.sendMsgBuf(0x1d3,0,8,dat2);


  //________________0xaa msg defaults 1a 6f WHEEL_SPEEDS
  uint8_t dat3[8];
  //uint16_t wheelspeed = 0x1a6f + (average * 100);
  uint16_t wheelspeed = 0x1a6f + (kmh_echt * 100);
  dat3[0] = (wheelspeed >> 8) & 0xFF;
  dat3[1] = (wheelspeed >> 0) & 0xFF;
  dat3[2] = (wheelspeed >> 8) & 0xFF;
  dat3[3] = (wheelspeed >> 0) & 0xFF;
  dat3[4] = (wheelspeed >> 8) & 0xFF;
  dat3[5] = (wheelspeed >> 0) & 0xFF;
  dat3[6] = (wheelspeed >> 8) & 0xFF;
  dat3[7] = (wheelspeed >> 0) & 0xFF;
  CAN_OP.sendMsgBuf(0xaa,0,8,dat3);

  //________________0x3b7 msg ESP_CONTROL
  uint8_t dat5[8];
  dat5[0] = 0x0;
  dat5[1] = 0x0;
  dat5[2] = 0x0;
  dat5[3] = 0x0;
  dat5[4] = 0x0;
  dat5[5] = 0x0;
  dat5[6] = 0x0;
  dat5[7] = 0x08;
  CAN_OP.sendMsgBuf(0x3b7,0,8,dat5);

  //________________0x620 msg STEATS_DOORS
  uint8_t dat6[8];
  dat6[0] = 0x10;
  dat6[1] = 0x0;
  dat6[2] = 0x0;
  dat6[3] = 0x1d;
  dat6[4] = 0xb0;
  dat6[5] = 0x40;
  dat6[6] = 0x0;
  dat6[7] = 0x0;
  CAN_OP.sendMsgBuf(0x620,0,8,dat6);

  //________________0x3bc msg GEAR_PACKET
  uint8_t dat7[8];
  dat7[0] = 0x0;
  dat7[1] = 0x0;
  dat7[2] = 0x0;
  dat7[3] = 0x0;
  dat7[4] = 0x0;
  dat7[5] = 0x80;
  dat7[6] = 0x0;
  dat7[7] = 0x0;
  CAN_OP.sendMsgBuf(0x3bc,0,8,dat7);


  //________________0x2c1 msg GAS_PEDAL
  uint8_t dat10[8];
  dat10[0] = (!gas_pedal_state << 3) & 0x08;
  dat10[1] = 0x0;
  dat10[2] = 0x0;
  dat10[3] = 0x0;
  dat10[4] = 0x0;
  dat10[5] = 0x0;
  dat10[6] = 0x0;
  dat10[7] = 0x0;
  CAN_OP.sendMsgBuf(0x2c1,0,8,dat10);

  //________________0x224 msg fake brake module
  uint8_t dat11[8];
  dat11[0] = 0x0;
  dat11[1] = 0x0;
  dat11[2] = 0x0;
  dat11[3] = 0x0;
  dat11[4] = 0x0;
  dat11[5] = 0x0;
  dat11[6] = 0x0;
  dat11[7] = 0x8;
  CAN_OP.sendMsgBuf(0x224,0,8, dat11);

  //________________send 0x262 fake EPS_STATUS
  uint8_t dat262[8];
  dat262[0] = 0x0;
  dat262[1] = 0x18;
  dat262[2] = 0x0;
  dat262[3] = (LKA_STATE << 7) & 0x40 | (TYPE << 0) & 0x1;
  dat262[3] = 0xB;
  dat262[4] = can_cksum(dat262, 4, 0x262);
  CAN_OP.sendMsgBuf(0x262,0,5, dat262);

  //________________0x262 fake EPS_STATUS
  uint8_t dat8[8];
  dat8[0] = 0x0;
  dat8[1] = 0x0;
  dat8[2] = 0x0;
  dat8[3] = 0x3;
  dat8[4] = 0x6c;
  //CAN.beginPacket(0x262);
  //for (int ii = 0; ii < 5; ii++) {
  //  CAN.write(dat8[ii]);
  //}
  CAN_OP.sendMsgBuf(0x25,0,8,dat8);


  //________________0x25 fake STEER_ANGLE
  uint8_t dat25[8];
  dat25[0] = 0x0f;
  dat25[1] = 0xff;
  dat25[2] = 0x00;
  dat25[3] = 0x01;
  dat25[4] = 0xc0;
  dat25[5] = 0x03;
  dat25[6] = 0x00;
  dat25[7] = 0xff;
  CAN_OP.sendMsgBuf(0x25,0,8,dat25);

  //________________0x260 fake STEER_TORQUE_SENSOR
  uint8_t dat9[8];
  dat9[0] = 0x08;
  dat9[1] = 0xff;
  dat9[2] = 0xfb;
  dat9[3] = 0x0;
  dat9[4] = 0x0;
  dat9[5] = 0xff;
  dat9[6] = 0xdc;
  dat9[7] = 0x47;
  CAN_OP.sendMsgBuf(0x260,0,8, dat9);

  //________________0x423 fake EPS message
  CAN_OP.sendMsgBuf(0x423,0,0,0);

  //________________0xb4 speed and encoder for throttle ECU
  uint16_t kmh = (average * 100);
  //CAN.sendMsgBuf(0xb4,0,0,kmh);


}
}

//TOYOTA CAN CHECKSUM
int can_cksum (uint8_t *dat, uint8_t len, uint16_t addr) {
  uint8_t checksum = 0;
  checksum = ((addr & 0xFF00) >> 8) + (addr & 0x00FF) + len + 1;
  //uint16_t temp_msg = msg;
  for (byte ii = 0; ii < len; ii++) {
    checksum += (dat[ii]);
  }
  return checksum;
}
