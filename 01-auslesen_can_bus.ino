void auslesen_can_bus_OP() {  

  if(enable_op_can){
    while (CAN_MSGAVAIL == CAN_OP.checkReceive()) {
      byte len1 = 0;
      byte buf[8];
      
      CAN_OP.readMsgBuf(&len1, buf);
      interrupt_counter++;
      
      switch (CAN_OP.getCanId()) {
        case 0x200:                             //GAS
          for (byte i = 0; i < len1; i++){
            x200_cansignal[i] = buf[i];  
          }
          break;
          
        case 0x343:                             //Bremse
          for (byte i = 0; i < len1; i++){
            x343_cansignal[i] = buf[i]; 
          }
          break;

        case 0x2E6:                             //LEAD CAR
          for (byte i = 0; i <= 7; i++) {
            x2E6_cansignal[i]  = buf[i];
          }
          break;
          
        default:
          for (byte i = 0; i < len1; i++){
            other_cansignal_op[i] = buf[i]; 
          }
          break;
      }
    }
    //delayMicroseconds(50);
  }
  }

void auslesen_can_bus_CAR() {  
  if(enable_car_can){
    byte        car_can[8];
    byte len1 = 0;
    while (CAN_MSGAVAIL == CAN_CAR.checkReceive()) {
      CAN_CAR.readMsgBuf(&len1, car_can);
      if(CAN_CAR.getCanId() == 0x348){
        for (byte i = 0; i < 8; i++){
          x348_car_cansignal[i] = car_can[i];  
        }  
      }
    }
  }
}
