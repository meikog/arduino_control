void eeprom_zero_all_tables(){
    wdt_disable();
    Serial.println(F(""));
    Serial.println(F("EEPROM WIRD GELÖSCHT!"));
    Serial.print("Länge: ");
    Serial.println(EEPROM.length());
    for (int b=0; b<EEPROM.length(); b = b + 1 ){
      EEPROM[b] = 0;
    };
    Serial.println(F("EEPROM wurde GELÖSCHT!"));
    Serial.println(F("INIT TABLE MULTI GAS SPEED/LOAD"));
    for (int b=0; b<256; b = b + 1 ){
      EEPROM[b] = 100;
    };
    Serial.println(F("INIT TABLE MULTI GAS SPEED/LOAD FINISHED 0-255"));
    Serial.println(F("BRAKE IS ALSO INIT BY ZERO 256-511"));
    delay(1200);
    wdt_enable(WDTO_2S);
    
}

void eeprom_read_2d_table_last_speed(){
//Serial.println(F("##ANFANG##"));
Serial.println(F("GAS MULTI VALUE DIV BY 100 e.g 100 =1; 50 = 0.5"));
// Tabelle von 0-255
//LOOP Durch Reihe
int eeprom_read_2d_table_last_speed_start = 0;                //Erste reihe im EEPROM!
Serial.println("");
for (byte a=0; a<16; a = a + 1 ){
  if(table_speed_arr[a] >= 0 && table_speed_arr[a] <10){
    Serial.print(F("  "));
  }
  if(table_speed_arr[a] >= 10 && table_speed_arr[a] <100){
    Serial.print(F(" "));
  }
  Serial.print(table_speed_arr[a]);
  Serial.print(F(","));

}
Serial.print(F(" Speed / Load"));
int start_array_range = eeprom_read_2d_table_last_speed_start;
for (byte a=0; a<16; a = a + 1 ){
  Serial.println();

  //LOOP durch Spalte
  for (byte b=0; b<16; b = b + 1 ){
     if(EEPROM[b + (a*16)] >= 0 && EEPROM[b + (a*16)] <10){
      Serial.print("  ");
    }
    if(EEPROM[b + (a*16)] >= 10 && EEPROM[b + (a*16)] <100){
      Serial.print(" ");
    }
   Serial.print(EEPROM[b + (a*16)]);
   Serial.print(F(","));
  };
  Serial.print(F(" || "));
  Serial.print(table_last_arr[a]);
  Serial.print(F(" || "));
  Serial.print(start_array_range);
  start_array_range = start_array_range + 15;
  Serial.print(F(" - "));
  Serial.print(start_array_range);
  start_array_range = start_array_range + 1;
  
};
Serial.println(F(""));
//Serial.println(F("##ENDE##"));
}
