void adc_read() {
float Averageadc1 = 0;
float Averageadc2 = 0;

int MeasurementsToAverage = 10;

for(byte i = 0; i < MeasurementsToAverage; ++i)
{
  Averageadc1 += analogRead(read_adc1);
  Averageadc2 += analogRead(read_adc2);
}

wert_adc1 = Averageadc1/MeasurementsToAverage; //1V
wert_adc2 = Averageadc2/MeasurementsToAverage;  //0.5V

}
