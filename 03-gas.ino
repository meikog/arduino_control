void gaspedal() {

//Steuerung Gaspedal
   if (flag2 && wert_adc1 <= 251  && wert_adc2 <= 126 && gas_stellung_raw > 0 ) { //Tempomat an und Gaspedal nicht benutzt wird
 /* 
      //To fast to lead car decrease gas by multi comma decrease to slow
    if(false){
        float lead_multi = 1;
        if(LEAD_REL_SPEED < -8){
          int calc constrain(LEAD_REL_SPEED, -19, 0);
          lead_multi = map(calc*-100, 800, 2000, 10000, 0);
          lead_multi /= 10000; 
        }
  
        //If distance to close gas will decrease by multi
        float distance_multi = 1;
        float target_distance = kmh_echt / 2.4;
        if(LEAD_LONG_DIST < target_distance  && LEAD_LONG_DIST > 1 && LEAD_REL_SPEED < 3  ){  // Wenn der wirkliche abstand kleiner ist als der geplante aber mindestens 1 Meter dann!
          int calc        = constrain(target_distance-LEAD_LONG_DIST, 0, 5);
          distance_multi  = map(calc, 0, 5, 100, 0);
          distance_multi  /= 100; 
        }
      }
  */
      wert_dac1 =idle_voltage_pedal1  + (idle_voltage_pedal1 * 2.4 * gas_stellung_raw / 100); //*gas_2d_multi*lead_multi*distance_multi
      tempomatadc1 = "on";
    }
    else {         
      tempomatadc1 = "off";                            
      wert_dac1 =(wert_adc1*4*1.20);
  }

  if (wert_dac1 > 3850){
    wert_dac1 = 3850;     // Wenn die Berechnung über 3500 geht maximal Limit
  }
  if(enable_mcp4728){
    CA = (wert_dac1+20) + (idle_correctur * 2);
    AA = (wert_dac1/2)  +  idle_correctur;
    if((wert_adc1< 251 && !flag2) || (flag2 == true && gas_von_ecu_can_raw == 0 && wert_adc1 <= 251  && wert_adc2 <= 126)){
      sensordac.setChannelValue(MCP4728_CHANNEL_C, idle_voltage_pedal1 + (idle_correctur * 2)  , MCP4728_VREF_INTERNAL, MCP4728_GAIN_2X); //960
      sensordac.setChannelValue(MCP4728_CHANNEL_A, idle_voltage_pedal2 + (idle_correctur)      , MCP4728_VREF_INTERNAL, MCP4728_GAIN_2X); //470
      idle = "idle";
    }else{
      sensordac.setChannelValue(MCP4728_CHANNEL_C, CA                                          , MCP4728_VREF_INTERNAL, MCP4728_GAIN_2X);
      sensordac.setChannelValue(MCP4728_CHANNEL_A, AA                                          , MCP4728_VREF_INTERNAL, MCP4728_GAIN_2X);
      idle = "an";

      
    }
  }
}
