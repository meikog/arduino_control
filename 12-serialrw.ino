void serialrw() {
  
  if (Serial.available() > 0) {
    
    String s1 = Serial.readStringUntil('\n');

    if (s1.lastIndexOf(F("help")) == 0) {
      update_display_enable = false;
      Serial.println(F(""));
      Serial.println(F("2D Table ADJUSTABLE!"));
      Serial.println(F("init table"));
      Serial.println(F("show gas"));
      Serial.println(F("show brake"));
      Serial.println(F("enable - default output was disabled by help to enable enter enable"));
      Serial.println(F("Edit value #adress=value e.g. #2=100"));
      Serial.println(F(""));
      delay(1000);
    } 

    if (s1.lastIndexOf("enable") == 0) {
      update_display_enable = true;
    } 

    if (s1.lastIndexOf(F("show gas")) == 0) {
      eeprom_read_2d_table_last_speed();
    } 
    if (s1.lastIndexOf("#") == 0) {
      Serial.println(s1.lastIndexOf("="));
      Serial.println(F("EEPROM SPEICHERFELD"));
      int eeprom_location = s1.substring(1, s1.lastIndexOf("=")).toInt();
      Serial.println(eeprom_location);
      Serial.println(F("SPEICHERWERT"));
      byte eeprom_value = s1.substring(s1.lastIndexOf("=")+1,-1).toInt();
      Serial.println(eeprom_value);
      EEPROM[eeprom_location] = eeprom_value;
    } 
    if (s1.lastIndexOf(F("init table")) == 0) {
      eeprom_zero_all_tables();
    } 

    if (s1.lastIndexOf(F("an")) == 0) {
      flag2 = 1;
      Serial.println(F("Tempomat.an"));
    } 
    if (s1.lastIndexOf(F("aus")) == 0) {
      flag2 = 0;
      Serial.println(F("Tempomat.aus"));
    }
    
    
    if (s1.lastIndexOf(F("lenkungan")) == 0) {
      Serial.println(F("Lenkung.an"));
    }
    if (s1.lastIndexOf(F("lenkungaus")) == 0) {
      Serial.println(F("Lenkung.aus"));
    }

    if (s1.lastIndexOf(F("bremsean")) == 0) {
      Serial.println(F("Bremse.an"));
    }
    if (s1.lastIndexOf("bremseaus") == 0) {
      Serial.println(F("Bremse.aus"));
    }
    
    /*if(s1.lastIndexOf("gps") >= 0){
    s1.remove(0, s1.lastIndexOf("gps")+3);
    Serial.println(s1);
    average = s1.toInt();
    Serial.print(F("GPS-SPEED: "));
    Serial.println(average);
    s1="";
    }
    */
    if (s1.lastIndexOf(F("now")) >= 0) {
      set_speed = ((int)round(kmh_echt/10))*10;
      Serial.println("Geschwindigkeit Jetzt" + set_speed);
    }
    if (s1.lastIndexOf(F("S55")) >= 0) {
      set_speed = 55;
      Serial.println(F("Geschwindigkeit 55"));
    }
    if (s1.lastIndexOf(F("S125")) >= 0) {
      set_speed = 125;
      Serial.println(F("Geschwindigkeit 125"));
    }
    if (s1.lastIndexOf(F("S85")) >= 0) {
      set_speed = 85;
      Serial.println(F("Geschwindigkeit 85"));
    }
    if (s1.lastIndexOf(F("speedincrease")) >= 0) {
      set_speed += 10;
      Serial.println("Geschwindigkeit " + set_speed);
    }
    if (s1.lastIndexOf(F("speeddecrease")) >= 0) {
      if (set_speed > 10) {
        set_speed -= 10;
      }
      Serial.println("Geschwindigkeit " + set_speed);
    }
    
   //#####BREMSE DEBUGGER
    if (s1.lastIndexOf(F("bremseincrease")) >= 0) {
        if (bremse_target_debugger < EEPROM[storage_brake_idle_pos]) {
          bremse_target_debugger += 5;
        }
      }
    if (s1.lastIndexOf(F("bremsedecrease")) >= 0) {
      if (bremse_target_debugger > bremse_max) {
        bremse_target_debugger -= 5;
      }
    }
    if (s1.lastIndexOf(F("bremsean")) >= 0) {
      bremse_debugger_an = true;
    }
    if (s1.lastIndexOf(F("bremseaus")) >= 0) {
      bremse_debugger_an = false;
    }
    if (s1.lastIndexOf(F("idleplus")) >= 0) {
      EEPROM[idle_correction_storage] = EEPROM[idle_correction_storage] + 1;
      idle_correctur = EEPROM[idle_correction_storage] - 127;
    }
    if (s1.lastIndexOf(F("idleminus")) >= 0) {
      EEPROM[idle_correction_storage] = EEPROM[idle_correction_storage] - 1;
      idle_correctur = EEPROM[idle_correction_storage] - 127;
    }
    if (s1.lastIndexOf(F("br200minus")) >= 0) {
      EEPROM[storage_brakem200] = EEPROM[storage_brakem200] - 1;
      update_brk_display();
    }
    if (s1.lastIndexOf(F("br400minus")) >= 0) {
      EEPROM[storage_brakem400] = EEPROM[storage_brakem400] - 1;
      update_brk_display();
    }
    if (s1.lastIndexOf(F("br600minus")) >= 0) {
      EEPROM[storage_brakem600] = EEPROM[storage_brakem600] - 1;
      update_brk_display();
    }
    if (s1.lastIndexOf(F("br800minus")) >= 0) {
      EEPROM[storage_brakem800] = EEPROM[storage_brakem800] - 1;
      update_brk_display();
    }
    if (s1.lastIndexOf(F("br1000minus")) >= 0) {
      EEPROM[storage_brakem1000] = EEPROM[storage_brakem1000] - 1;
      update_brk_display();
    }
    if (s1.lastIndexOf(F("br1200minus")) >= 0) {
      EEPROM[storage_brakem1200] = EEPROM[storage_brakem1200] - 1;
      update_brk_display();
    }
    if (s1.lastIndexOf(F("br1400minus")) >= 0) {
      EEPROM[storage_brakem1400] = EEPROM[storage_brakem1400] - 1;
      update_brk_display();
    }
    if (s1.lastIndexOf(F("br1600minus")) >= 0) {
      EEPROM[storage_brakem1600] = EEPROM[storage_brakem1600] - 1;
      update_brk_display();
    }
    if (s1.lastIndexOf(F("br1800minus")) >= 0) {
      EEPROM[storage_brakem1800] = EEPROM[storage_brakem1800] - 1;
      update_brk_display();
    }
    if (s1.lastIndexOf(F("br2000minus")) >= 0) {
      EEPROM[storage_brakem2000] = EEPROM[storage_brakem2000] - 1;
      update_brk_display();
    }

    if (s1.lastIndexOf(F("br200plus")) >= 0) {
      EEPROM[storage_brakem200] = EEPROM[storage_brakem200] + 5;
      update_brk_display();
    }
    if (s1.lastIndexOf(F("br400plus")) >= 0) {
      EEPROM[storage_brakem400] = EEPROM[storage_brakem400] + 5;
      update_brk_display();
    }
    if (s1.lastIndexOf(F("br600plus")) >= 0) {
      EEPROM[storage_brakem600] = EEPROM[storage_brakem600] + 5;
      update_brk_display();
    }
    if (s1.lastIndexOf(F("br800plus")) >= 0) {
      EEPROM[storage_brakem800] = EEPROM[storage_brakem800] + 5;
      update_brk_display();
    }
    if (s1.lastIndexOf(F("br1000plus")) >= 0) {
      EEPROM[storage_brakem1000] = EEPROM[storage_brakem1000] + 5;
      update_brk_display();
    }
    if (s1.lastIndexOf(F("br1200plus")) >= 0) {
      EEPROM[storage_brakem1200] = EEPROM[storage_brakem1200] + 5;
      update_brk_display();
    }
    if (s1.lastIndexOf(F("br1400plus")) >= 0) {
      EEPROM[storage_brakem1400] = EEPROM[storage_brakem1400] + 5;
      update_brk_display();
    }
    if (s1.lastIndexOf(F("br1600plus")) >= 0) {
      EEPROM[storage_brakem1600] = EEPROM[storage_brakem1600] + 5;
      update_brk_display();
    }
    if (s1.lastIndexOf(F("br1800plus")) >= 0) {
      EEPROM[storage_brakem1800] = EEPROM[storage_brakem1800] + 5;
      update_brk_display();
    }
    if (s1.lastIndexOf(F("br2000plus")) >= 0) {
      EEPROM[storage_brakem2000] = EEPROM[storage_brakem2000] + 5;
      update_brk_display();
    }
    
    if (s1.lastIndexOf(F("brakemaxplus")) >= 0) {
      EEPROM[storage_brake_max] = EEPROM[storage_brake_max] + 5;
      update_brk_display();
    }
    if (s1.lastIndexOf(F("brakemaxminus")) >= 0) {
      EEPROM[storage_brake_max] = EEPROM[storage_brake_max] - 5;
      update_brk_display();
    }

    //Brake Adjustment
    if (s1.lastIndexOf(F("bremseidlepos_min")) >= 0) {
      EEPROM[storage_brake_idle_pos] = EEPROM[storage_brake_idle_pos] - 1;
      update_brk_display();
    }
    if (s1.lastIndexOf(F("bremseidlepos_plus")) >= 0) {
      EEPROM[storage_brake_idle_pos] = EEPROM[storage_brake_idle_pos] + 1;
      update_brk_display();
    }
    if (s1.lastIndexOf(F("bremseminpos_plus")) >= 0) {
      EEPROM[storage_brake_min_pos] = EEPROM[storage_brake_min_pos]  + 1;
      update_brk_display();
    }
    if (s1.lastIndexOf(F("bremseminpos_min")) >= 0) {
      EEPROM[storage_brake_min_pos] = EEPROM[storage_brake_min_pos] - 1;
      update_brk_display();
    }

        
    //refreshbrk
    if (s1.lastIndexOf(F("refreshbrk")) >= 0) {
      update_brk_display();
    }
    if (s1.lastIndexOf(F("debuggerpage")) >= 0) {
      debugger_page_name();
    }
    
    
  }
}
